/**
 * Client
 */
const path = require('path')
const express = require('express')
const app = express()
// set the view engine to ejs
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'))
app.use(express.static(path.join(__dirname, 'views')))

// index page
app.get('/', function(req, res) {
  res.render('pages/index');
});

const cote = require('cote')
const os = require('os')
const requester = new cote.Requester({ name: `Controller-Requester-${os.hostname()}` })

app.get('/getRandomSystemInfo', function (req, res) {
  requester.send({ type: 'systemInfo', hostname: os.hostname() }, (hostInfo) => {
    res.send({
      data: hostInfo
    })
  })
})


app.listen(3000)