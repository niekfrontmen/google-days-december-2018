$(document).ready(function(){
  $( "#btn-getSystemInfo" ).click(function() {
    $.get( "http://localhost:3000/getRandomSystemInfo", function( response ) {
      $( "#hostname" ).html( response.data.hostname );
      $( "#type" ).html( response.data.type );
      $( "#platform" ).html( response.data.platform );
      $( "#arch" ).html( response.data.arch );
      $( "#release" ).html( response.data.release );
      $( "#uptime" ).html( response.data.uptime );
      $( "#loadavg" ).html( response.data.loadavg );
      $( "#totalmem" ).html( response.data.totalmem );
      $( "#freemem" ).html( response.data.freemem );
      $( "#cpus" ).html( response.data.cpus.length );
    });
  });
});