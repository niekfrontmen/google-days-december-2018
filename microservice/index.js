/**
 * Micro Service
 */
const os = require('os')
const cote = require('cote')
const responder = new cote.Responder({ name: `Service-Responder-${os.hostname()}` })

responder.on('systemInfo', (req, res) => {
  console.log(`You've received a Request from ${req.hostname} `)
  res({
    hostname: os.hostname(),
    type: os.type(),
    platform: os.platform(),
    arch: os.arch(),
    release: os.release(),
    uptime: os.uptime(),
    loadavg: os.loadavg(),
    totalmem: os.totalmem(),
    freemem: os.freemem(),
    cpus: os.cpus(),
    network_interfaces: os.networkInterfaces()
  })
})